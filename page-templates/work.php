<?php
/*
Template Name: Work
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hidden Breadcrumb Data -->

			<div class="breadcrumb_info" data-link="<?php echo esc_url( home_url( '/' ) ); ?>" data-text="Back To Main"></div>

			<!-- Page Header -->

			<?php 

			$posts = get_field('featured_work');

			if($posts): ?>

				<!-- Featured Work Slider -->

				<div class="flexslider mas_slider header_slider mas_header">

					<ul class="slides">

					    <?php 
					    // Setting a count here so we only pull in 3 featured work posts
					    $featured_posts_count = 0;
					    foreach( $posts as $post): ?>
					    	<?php if($featured_posts_count === 3): ?>
					    		<?php break; ?>
					    	<?php else: ?>
						        <?php setup_postdata($post); ?>
						        <?php
								// If the posts header image is set then display it
								if(get_field('header_image')) {
									$header_image_url = image_id_to_url(get_field('header_image'), 'large');
								}
								// Else just use the featured image
								else {
									$header_image_url = get_the_post_thumbnail_url($post->ID, 'large');
								}
								?>
								<li style="background-image: url(<?php echo $header_image_url ?>);">
									<a href="<?php the_permalink(); ?>">
										<div class="header_overlay_content">
											<div class="small_container">
												<h4>Featured</h4>
												<h1><?php the_title(); ?></h1>
											</div>
										</div>
									</a>
								</li>
							<?php endif; ?>
					    <?php 
					    $featured_posts_count++;
					    endforeach; ?>

			    	</ul>

			    </div>

			<?php wp_reset_postdata(); endif; ?>

			<!-- Page Content -->

			<div class="page_content">

				<div class="content_fade_in">

					<div class="mas_panel">
						
						<div class="container work_mixitup_container">

							<div class="mobile_mixitup_filter">
								<button>Filter By <span class="mobile_filter_title">All</span></button>
							</div>

							<!-- Work Filters -->
				
							<ul class="mixitup_filters">
								<li class="filter" data-filter="all">All Work</li>
								<?php
								// Get all of the terms within the work categories taxonomy
								$args = array('hide_empty' => true, 'taxonomy' => 'mas_work_categories');
								$categories = get_terms($args);
								// Loop through each term to output a mixitup filter
								foreach($categories as $cat){ 
								?>	
									<li class="filter" data-filter="<?php echo '.' . $cat->slug ?>"><?php echo $cat->name ?></li>
								<?php } ?>
								<li><a href="<?php echo get_page_link(574); ?>">Film & TV</a></li>
							</ul>

							<!-- Work Grid -->

							<div class="mas_row work_grid">
								<?php

								// Loop through all of the work posts.

								$work_posts_args = array('post_type' => 'mas_work', 'posts_per_page' => -1);
								$work_posts_loop = new WP_Query($work_posts_args);
								if ($work_posts_loop->have_posts()) : while ($work_posts_loop->have_posts()) : $work_posts_loop->the_post();
								?>
									<?php get_template_part('template-parts/card', 'work_item'); ?>

								<?php endwhile; endif; wp_reset_postdata(); ?>

							</div>

							<!-- Work Pagination -->

							<div class="mixitup-page-list"></div>

						</div>

					</div>

				</div>

			</div>
			
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>