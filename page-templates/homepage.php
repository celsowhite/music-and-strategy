<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hero Image -->

			<?php 

				// Random header image output in the hero of the page

				$header_image_rows = get_field('hero_header_images' );
				$rand_header_image_row = $header_image_rows[array_rand( $header_image_rows)];
				$header_image = $rand_header_image_row['image' ];
				$header_horizontal_focal_point = $rand_header_image_row['horizontal_focal_point' ];
				$header_vertical_focal_point = $rand_header_image_row['vertical_focal_point' ];
			?>

			<header class="page_header tall homepage_header mas_header" style="background-image:url(<?php echo image_id_to_url($header_image, 'large'); ?>); background-position:<?php echo $header_horizontal_focal_point . ' ' . $header_vertical_focal_point; ?>;">
				<div class="container">
					<img src="<?php echo get_template_directory_uri() . '/img/logo/logo_large_white.svg' ?>" class="large_logo" />
				</div>
			</header>

			<!-- Page Content -->

			<div class="page_content">

				<!-- About -->

				<section class="homepage_about">
					<div class="mas_panel large">
						<div class="small_container text_center with_text_breaks">
							<h1 class="large"><?php the_field('homepage_about_content'); ?></h1>
							<a class="arrow_link black" href="<?php the_field('homepage_about_link_url'); ?>"><?php the_field('homepage_about_link_text'); ?></a>
						</div>
					</div>
				</section>

				<!-- Reel -->

				<section class="mas_video_container">
					<div class="video_cover" style="background-image:url(<?php the_field('reel_placeholder_image'); ?>);">
						<div class="content">
							<h1><?php the_field('reel_header'); ?></h1>
							<img class="play_video" src="<?php echo get_template_directory_uri() . '/img/icons/play_button_white.svg'; ?>" />
						</div>
					</div>
					<div data-type="<?php the_field('video_embed_type'); ?>" data-video-id="<?php the_field('video_embed_id'); ?>"></div>
				</section>

				<!-- Featured Work -->

				<section class="homepage_featured_work">
					<div class="mas_panel">
						<div class="container">

							<h1 class="title_header large">Featured Work</h1>

							<?php 

							$posts = get_field('featured_work', 72);

							if($posts): ?>

								<div class="mas_row work_grid">

								    <?php foreach( $posts as $post): ?>
								        
								        <?php get_template_part('template-parts/card', 'work_item'); ?>

								    <?php endforeach; ?>

								</div>

							<?php wp_reset_postdata(); endif; ?>
							<div class="text_center">
								<a class="arrow_link black" href="<?php echo get_page_link(72); ?>">View All Work</a>
							</div>
						</div>
					</div>
				</section>

				<!-- Get Inspired -->

				<section class="homepage_get_inspired">
					<div class="mas_panel background_grey">
						<div class="container text_center">
							<h1 class="title_header large">Get Inspired</h1>
							<!-- Instagram Feed -->
							<div class="instafeed_container" id="instafeed"></div>
							<a class="arrow_link black" href="<?php echo get_page_link(74); ?>">View Our Inspirations</a>
						</div>
					</div>
				</section>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
