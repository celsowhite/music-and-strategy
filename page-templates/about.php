<?php
/*
Template Name: About
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hidden Breadcrumb Data -->

			<div class="breadcrumb_info" data-link="<?php echo esc_url( home_url( '/' ) ); ?>" data-text="Back To Main"></div>

			<!-- Page Header -->

			<?php get_template_part('template-parts/component', 'page_header'); ?>

			<!-- Page Content -->

			<div class="page_content">

				<div class="small_container wysiwig content_fade_in">

					<div class="mas_panel">

						<?php get_template_part('template-parts/acf', 'page_components'); ?>

					</div>

				</div>

				<!-- Featured Work Grid -->

				<div class="mas_panel background_white">
					<div class="container">
						<header class="title_header">
							<h1><?php the_field('about_work_title'); ?></h1>
						</header>
						<div class="mas_row work_grid">
							<?php

							// Query three random work pieces

							$work_posts_args = array('post_type' => 'mas_work', 'posts_per_page' => 3, 'orderby' => 'rand');
							$work_posts_loop = new WP_Query($work_posts_args);
							if ($work_posts_loop->have_posts()) : while ($work_posts_loop->have_posts()) : $work_posts_loop->the_post();
							?>
								<?php get_template_part('template-parts/card', 'work_item'); ?>

							<?php endwhile; endif; wp_reset_postdata(); ?>
						</div>
						<div class="text_center">
							<a class="arrow_link black" href="<?php echo get_page_link(72); ?>">View All Work</a>
						</div>
					</div>
				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

