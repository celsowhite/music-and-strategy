<?php
/*
Template Name: Inspirations
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hidden Breadcrumb Data -->

			<div class="breadcrumb_info" data-link="<?php echo esc_url( home_url( '/' ) ); ?>" data-text="Back To Main"></div>

			<!-- Page Header -->

			<?php
			// Most recent inspiration post
			$recent_inspiration_post_args = array ('post_type' => 'post', 'posts_per_page' => 1);
			$recent_inspiration_post_loop = new WP_Query($recent_inspiration_post_args);
			if($recent_inspiration_post_loop -> have_posts()): while($recent_inspiration_post_loop -> have_posts()): $recent_inspiration_post_loop -> the_post();
			?>	
				<?php
					// If the posts header image is set then display it
					if(get_field('header_image')) {
						$header_image_url = image_id_to_url(get_field('header_image'), 'large');
					}
					// Else just use the featured image
					else {
						$header_image_url = get_the_post_thumbnail_url();
					}
				?>
				<a href="<?php the_permalink(); ?>">
					<header class="page_header mas_header" style="background-image:url(<?php echo $header_image_url ?>);">
						<div class="header_overlay_content">
							<div class="small_container">
								<h4><?php echo get_the_date( 'd.m.y' ) ?></h4>
								<h1><?php the_title(); ?></h1>
							</div>
						</div>
					</header>
				</a>	
			<?php endwhile; wp_reset_postdata(); endif; ?>

			<!-- Page Content -->

			<div class="page_content">

				<div class="content_fade_in">

					<div class="mas_panel">
						
						<div class="small_container">

							<?php the_content(); ?>

							<!-- MAS Inspirations Posts -->
							
							<?php 
							
							// Inspiration posts grid. Shortcode via AJAX Load more plugin. Template stored in theme directory alm_templates/alm-inspiration_grid

							echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="4" theme_repeater="alm-inspiration_grid.php" transition="fade" order="desc" orderby="date" scroll="false" offset="1" button_label="load more posts"]'); ?>
							
							<?php
								// Save Twitter and IG links
								// Set via the backend options page
								while(have_rows('mas_social_profiles', 'option')): the_row();
									if(get_sub_field('name') === 'Twitter') {
										$twitter_link = get_sub_field('link');
									}
									else if(get_sub_field('name') === 'Instagram') {
										$instagram_link = get_sub_field('link');
									}
								endwhile;
							?>
							<!-- Twitter Feed -->

							<div class="mas_panel medium text_center">
								<div id="mas_recent_tweet"></div>
								<?php if(have_rows('mas_social_profiles', 'option')): ?>
									<?php while(have_rows('mas_social_profiles', 'option')): the_row(); ?>

									<?php endwhile; ?>
								<?php endif ;?>
								<a class="arrow_link black" href="<?php echo $twitter_link; ?>" target="_blank">Twitter</a>
							</div>

						</div>

						<div class="container">

							<!-- IG Feed -->

							<div class="text_center">
								<div class="instafeed_container" id="instafeed"></div>
								<a class="arrow_link black" href="<?php echo $instagram_link; ?>" target="_blank">Instagram</a>
							</div>

						</div>

					</div>

				</div>

			</div>
			
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>