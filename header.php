<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" />

<?php wp_head(); ?>
</head>

<?php
// Set page style settings
if(get_field('page_color_scheme')) {
	$page_color_scheme = get_field('page_color_scheme');
}
else {
	$page_color_scheme = '';
}

if(is_single()) {
	$page_color_scheme = 'light_background';
}

if(get_field('hero_header_images') || (get_field('header_image') && !is_archive()) || get_field('featured_work') || is_page(74)) {
	$hasHeader = 'has_header_image';
}
else {
	$hasHeader = '';
}

?>

<body <?php body_class(array($page_color_scheme, $hasHeader)); ?>>

	<!-- Main Header -->

	<header class="main_header">
		<ul class="main_header_left">
			<li class="fly_out_menu_trigger">Menu</li>
			<li class="mas_breadcrumb"></li>
		</ul>
		<div class="main_header_right">
			<div class="currently_listening_to"></div>
			<?php /*
				Currently Listening To: <span class="currently_listening_track"><span class="currently_listening_track_name"></span><span class="currently_listening_artist"></span></span>
			</div> */ ?>
			<div class="follow_link">
	            <span>Follow</span>
	            <?php get_template_part('template-parts/component', 'social_profiles'); ?>
	        </div>
	    </div>
	</header>

	<!-- Fly Out Menu -->

	<section class="fly_out_menu">
		<div class="fly_out_menu_container">

			<!-- Logo -->

			<header class="fly_out_menu_header">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo get_template_directory_uri() . '/img/logo/logo_full_white.svg' ?>" class="fly_out_menu_logo" />
				</a>
				<span class="close_icon"></span>
			</header>

			<!-- Main Menu -->

			<?php wp_nav_menu( array( 'menu' => 'main-menu', 'menu_class' => 'fly_out_menu_navigation', 'container' => '' ) ); ?>

			<!-- Addresses -->

			<footer>
				<div class="tabs_container address_toggle">
				    <ul class="tab_items">
				    	<?php if(have_rows('mas_addresses', 'option')): while(have_rows('mas_addresses', 'option')): the_row(); ?>
							<li data-title="<?php the_sub_field('location'); ?>"><?php the_sub_field('location'); ?></li>
							<span>/</span>
						<?php endwhile; endif; ?>
				    </ul>
				    <div class="tab_content">
				    	<?php if(have_rows('mas_addresses', 'option')): while(have_rows('mas_addresses', 'option')): the_row(); ?>
							<div id="<?php the_sub_field('location'); ?>"><?php echo wp_strip_all_tags(get_sub_field('address')); ?></div>
						<?php endwhile; endif; ?>
				    </div>
				</div>
			</footer>
		</div>
	</section>

	<div class="page_opacity_overlay"></div>

	<div id="content" class="main_content">