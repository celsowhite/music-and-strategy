<?php
	
	function MAS() {

		// Set up the API and OAuth flow
		// Using client credentials flow because we don't require any user interaction or access to personal data.
		// Wrapper: https://github.com/jwilsson/spotify-web-api-php/
		// Wrapper documentation for making spotify web api calls: https://github.com/jwilsson/spotify-web-api-php/blob/master/docs/method-reference/SpotifyWebAPI.md
		
		require_once('spotify-web-api-php/src/Request.php');
		require_once('spotify-web-api-php/src/Session.php');
		require_once('spotify-web-api-php/src/SpotifyWebAPI.php');
		require_once('spotify-web-api-php/src/SpotifyWebAPIException.php');

		$session = new SpotifyWebAPI\Session(
		    'eb5bc53b185a4a52a57cca45d0666984',
		    '984e0ec38b3e401392ccf03a095ec29d'
		);

		$api = new SpotifyWebAPI\SpotifyWebAPI();

		$session->requestCredentialsToken();

		$accessToken = $session->getAccessToken();

		$api->setAccessToken($accessToken);

		// Get MAS's playlists
		
		$playlists = $api->getUserPlaylists('spotify:user:music_and_strategy');

		// Get the most recent playlist

		$recentPlaylistID = $playlists->items[0]->id;

		// Get the tracks within the most recent playlist

		$tracks = $api->getUserPlaylistTracks('spotify:user:music_and_strategy', $recentPlaylistID);

		$tracksJSON = json_encode($tracks->items);

		echo $tracksJSON;

		wp_die();

	}

	add_action('wp_ajax_nopriv_MAS', 'MAS');
	add_action( 'wp_ajax_MAS', 'MAS' );
	
?>