<?php

/*---------------------
Dashboard Menu Adjustments
---------------------*/

// Only hide specific dashboard menu items on the live server.
// Show on localhost so we can control every component.

$whitelist = array('127.0.0.1','::1');

if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){

	// Remove Menu Pages

	function my_remove_menu_pages() {
		remove_menu_page('wpseo_dashboard');
		remove_menu_page('edit-comments.php');          
		remove_menu_page('ajax-load-more');              
	};

	add_action( 'admin_menu', 'my_remove_menu_pages', 999 );
}

/*---------------------
Dashboard Main Screen Adjustments
---------------------*/

/*--- Add new dashboard widget ---*/

// Function that outputs the contents of the dashboard widget

function dashboard_widget_function( $post, $callback_args ) {
	echo '<img src="' . get_template_directory_uri() . '/screenshot.png" style="width: 100%;" />';
	echo '<p>Welcome to the Music and Strategy dashboard. From this area you can edit posts, pages and other components of the site.</p><p>Please be sure to update Wordpress and the Plugins. A notice will appear in this dashboard when updates are available.</p>';
}

// Function used in the action hook

function add_dashboard_widgets() {
	wp_add_dashboard_widget('dashboard_widget', 'Music And Strategy Dashboard', 'dashboard_widget_function');
}

// Register the new dashboard widget with the 'wp_dashboard_setup' action

add_action('wp_dashboard_setup', 'add_dashboard_widgets' );

/*--- Remove Default Widgets ---*/

add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

function remove_dashboard_widgets() {
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
	remove_meta_box('dashboard_primary', 'dashboard', 'side');
	remove_meta_box('dashboard_secondary', 'dashboard', 'side');
	remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'side');
	remove_meta_box('rg_forms_dashboard', 'dashboard', 'side');
}

/*--- Remove Welcome Panel ---*/

remove_action('welcome_panel', 'wp_welcome_panel');

?>