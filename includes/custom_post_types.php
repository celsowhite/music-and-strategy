<?php

/*==========================================
WORK
==========================================*/

// Post Type

function custom_post_type_work() {

	$labels = array(
		'name'                => ('Work'),
		'singular_name'       => ('Work'),
		'menu_name'           => ('Work'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Work'),
		'view_item'           => ('View Work'),
		'add_new_item'        => ('Add New Work'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Work'),
		'update_item'         => ('Update Work'),
		'search_items'        => ('Search Work'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Work'),
		'description'         => ('Work'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'work'),
		'taxonomies'          => array('mas_work_categories', 'mas_work_tags', 'mas_work_clients'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-format-audio',
	);

	register_post_type( 'mas_work', $args );

}

add_action( 'init', 'custom_post_type_work', 0 );

// Work Category

function add_work_category() {
	$labels = array(
		'name' => ('Category'),
      	'singular_name' => ('Category'),
      	'search_items' =>  ('Search Categories' ),
      	'all_items' => ('All Categories' ),
      	'parent_item' => ('Parent Category' ),
      	'parent_item_colon' => ('Parent Category:' ),
      	'edit_item' => ('Edit Category' ),
      	'update_item' => ('Update Category' ),
      	'add_new_item' => ('Add New Category' ),
      	'new_item_name' => ('New Category' ),
      	'menu_name' => ('Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'work-category' )
	);

	register_taxonomy( 'mas_work_categories', array('mas_work'), $args );
}

add_action( 'init', 'add_work_category', 0 );

// Work Clients

function add_work_clients() {
	$labels = array(
		'name' => ('Clients'),
      	'singular_name' => ('Client'),
      	'search_items' =>  ('Search Clients' ),
      	'all_items' => ('All Clients' ),
      	'parent_item' => ('Parent Client' ),
      	'parent_item_colon' => ('Parent Client:' ),
      	'edit_item' => ('Edit Client' ),
      	'update_item' => ('Update Client' ),
      	'add_new_item' => ('Add New Client' ),
      	'new_item_name' => ('New Client' ),
      	'menu_name' => ('Clients' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'work-client' )
	);

	register_taxonomy( 'mas_work_clients', array('mas_work'), $args );
}

add_action( 'init', 'add_work_clients', 0 );

// Work Tags

function add_work_tags() {
	$labels = array(
		'name' => ('Tags'),
      	'singular_name' => ('Tag'),
      	'search_items' =>  ('Search Tags' ),
      	'all_items' => ('All Tags' ),
      	'parent_item' => ('Parent Tag' ),
      	'parent_item_colon' => ('Parent Tag:' ),
      	'edit_item' => ('Edit Tag' ),
      	'update_item' => ('Update Tag' ),
      	'add_new_item' => ('Add New Tag' ),
      	'new_item_name' => ('New Tag' ),
      	'menu_name' => ('Tags' ),
	);

	$args = array(
		'hierarchical'      => false,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'work-tag' )
	);

	register_taxonomy( 'mas_work_tags', array('mas_work'), $args );
}

add_action( 'init', 'add_work_tags', 0 );

?>