<?php

/*==========================================
FULLWIDTH
==========================================*/

function mas_fullwidth_shortcode( $atts, $content = null ) {

    return '</div><div class="small_container">' . $content . '</div><div class="single_post_container">';

}

add_shortcode( 'fullwidth', 'mas_fullwidth_shortcode' );

/*==========================================
BUTTON
==========================================*/

function nyss_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'url'    => '',
        'new_tab' => 'false',
        'color'  => 'studio_school_blue'
    ), $atts );

    if($a['new_tab'] === 'true') {
        $new_tab = 'target="_blank"';
    }
    else {
        $new_tab = '';
    }

    return '<a href="' . $a['url'] . '"' . $new_tab . ' class="nyss_button ' . $a['color'] . '">' . $content . '</a>';

}

add_shortcode( 'nyss_button', 'nyss_button_shortcode' );

/*==========================================
BLOCKQUOTE
==========================================*/

function nyss_blockquote_shortcode( $atts, $content = null ) {

    return '<div class="nyss_blockquote"><h3>' . $content . '</h3></div>';

}

add_shortcode( 'nyss_blockquote', 'nyss_blockquote_shortcode' );

/*==========================================
NYSS SOCIAL PROFILES
==========================================*/

function nyss_social_profiles_shortcode($atts, $content = null) {
    ob_start();
    get_template_part('template-parts/component', 'social_profiles');
    return ob_get_clean();
}

add_shortcode('nyss_social_profiles', 'nyss_social_profiles_shortcode');

?>