(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Homepage
		=================================*/

		const homepageHeader = $('.homepage_header');

		const homepageHeaderLogo = $('.homepage_header img');

		// Fade in the logo

		window.setTimeout(function(){
			homepageHeader.addClass('logo_fade_in');
		}, 600);

		// Fade out the overlay

		window.setTimeout(function(){
			homepageHeader.addClass('overlay_fade_out');
		}, 1500);

		/*================================= 
		Page Content
		=================================*/

		if($('body').hasClass('has_header_image') && !$('body').hasClass('home')) {
			window.setTimeout(function(){
				$('body').addClass('reveal_header_image');
			}, 500);
			window.setTimeout(function(){
				$('body').addClass('loaded');
			}, 1000);
		}
		else {
			window.setTimeout(function(){
				$('body').addClass('loaded');
			}, 500);
		}

	});

})(jQuery);