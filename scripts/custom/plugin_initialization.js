(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Tabify
		https://github.com/celsowhite/tabify
		=================================*/

		$('.tabs_container').tabify();

		/*================================= 
		Clipboard
		https://github.com/zenorocha/clipboard.js
		=================================*/

		const clipboard = new Clipboard('.clipboard');

		clipboard.on('success', function(e) {
			const savedClipboardText = e.trigger.innerHTML;
			e.trigger.innerHTML = 'Copied';
			window.setTimeout(function(){
				e.trigger.innerHTML = savedClipboardText;
			}, 1000);
		});

		clipboard.on('error', function(e) {
		    console.error(e);
		});

		/*================================= 
		FLEXSLIDER
		=================================*/

		$('.caption_slider').flexslider({
	    	animation: "fade",
	    	controlNav: false,
	    	directionNav: false
	    });

		$('.mas_slider').flexslider({
	    	animation: "fade",
	    	controlNav: true,
	    	directionNav: false,
	    	smoothHeight: false,
	    	sync: '.caption_slider'
	    });

	});

})(jQuery);