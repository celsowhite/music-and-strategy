(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		FOLLOW LINK REVEAL
		=================================*/

		const followLink = document.querySelector('.follow_link');

		function showFollowLinks() {
			this.classList.add('open');
		} 

		function hideFollowLinks() {
			this.classList.remove('open');
		}

		followLink.addEventListener('mouseenter', showFollowLinks);

		followLink.addEventListener('mouseleave', hideFollowLinks);

		/*================================= 
		Fly Out Menu Reveal
		=================================*/

		const flyOutMenu = $('.fly_out_menu');
		const flyOutMenuTrigger = $('.fly_out_menu_trigger');
		const flyOutMenuClose = $('.fly_out_menu .close_icon');

		const pageOverlay = $('.page_opacity_overlay');

		// Open/Close Fly Out Menu
		// Also toggle the active status of the page overlay that appears
		
		function toggleFlyOutMenu() {
			flyOutMenu.toggleClass('open');
			pageOverlay.toggleClass('active');
		}

		flyOutMenuTrigger.click(toggleFlyOutMenu);

		flyOutMenuClose.click(toggleFlyOutMenu);

		/*================================= 
		Work Link Reveal Sub Menu
		=================================*/

		const workLink = document.querySelector('.work_menu_item > a');

		const workSubMenu = document.querySelector('.work_menu_item ul.sub-menu');

		function showWorkSubMenu(e) {
			e.preventDefault();
			workSubMenu.classList.toggle('open');
		}

		workLink.addEventListener('click', showWorkSubMenu);
		
		/*================================= 
		Fixed Navigation
		=================================*/

		const header = document.querySelector('.main_header');

		function adjustHeader() {
			if(window.scrollY >= 30) {
				header.classList.add('scrolled');
			}
			else {
				header.classList.remove('scrolled');
			}
		}

		window.addEventListener('scroll', adjustHeader);

		/*================================= 
		HEADER COLOR

		// Change the header menu color if there is a header image from white to black.
		// Makes main navigation legible on top of header image.

		const mainHeader = document.querySelector('.main_header');
		const pageContent = document.querySelector('.page_content');
		const headerImage = document.querySelector('.mas_header');

		// Wait to run the function in case we have a flexslider in the header and it's height takes a bit to calculate.

		window.setTimeout(function(){
			if(headerImage) {

				// Calculate the offset of the page content. So we know when to transition the header color.
				
				const pageContentOffset = pageContent.offsetTop;

				function transformHeader(e) {
					if(window.scrollY >= pageContentOffset) {
						mainHeader.classList.add('dark');
					}
					else {
						mainHeader.classList.remove('dark');
					}
				}

				window.addEventListener('scroll', transformHeader);
			}
		}, 1000);

		=================================*/

		/*================================= 
		BREADCRUMB
		Each page that we want a breadcrumb we put some hidden breadcrumb information.
		That information is pulled via the below function and then set in the main header.
		=================================*/

		const breadcrumb = document.querySelector('.mas_breadcrumb');
		const breadcrumbInfo = document.querySelector('.breadcrumb_info');

		if(breadcrumbInfo) {

			// Save the info

			const breadcrumbLink = breadcrumbInfo.dataset.link;
			const breadcrumbText = breadcrumbInfo.dataset.text;

			// Set the pages breadcrumb

			breadcrumb.innerHTML = '<a href="' + breadcrumbLink + '">' + breadcrumbText + '</a>';
		}

	});

})(jQuery);