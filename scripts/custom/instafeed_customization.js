(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		INSTAFEED
		http://instafeedjs.com/
		=================================*/

		var masInstafeed = new Instafeed({
			clientId: '4a301de468904adea585cd377ee7f6ca',
			accessToken: '595709037.4a301de.1e2798b29b784f0fbc04d048eb75f550',
	        get: 'user',
	        userId: '595709037',
	        template: '<div class="instafeed_item_container"><a href="{{link}}" target="_blank" class="instafeed_item" style="background-image: url({{image}});"><div class="instafeed_caption"><h4>{{model.created_time}}</h4><p>{{caption}}</p></div></a></div>',
	        limit: 3,
	        resolution: 'standard_resolution',
	        filter: function(image) {

	        	// Convert instagram timestamp date to regular date format

				var date = new Date(image.created_time*1000);

				var m = ("0" + (date.getMonth() + 1)).slice(-2);
				var d = date.getDate();
				var y = parseInt(date.getFullYear().toString().substr(2,2), 10);

				var thetime = m + '.' + d + '.' + y;

				image.created_time = thetime;

				// Trim instagram caption

				if(image.caption) {
					var caption = image.caption.text;

					var captionLength = caption.length;

					// If the caption is longer than 140 characters then trim it.
					if(captionLength > 140) {
					  var caption = caption.slice(0, 140);
					  image.caption.text = caption + '...';
					}
					// Else return the original caption
					else {
						image.caption.text = caption;
					}
				}

				return true;
			}
		});

	    if($('#instafeed').length > 0) {
	    	masInstafeed.run();
	    }

	});

})(jQuery);