(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Plyr Initialization
		https://github.com/sampotts/plyr
		=================================*/

		// Initialize the players on the page

		const players = plyr.setup({
			controls: ['play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen']
		});

		/*================================= 
		Custom play button to trigger plyr
		For plyr instances with a custom cover image
		=================================*/

		// Find all the plyr nodes on the page

		const plyrInstances = $('.plyr');

		const playButton = $('.play_video');

		// On click of a specific play button

		playButton.click(function(){

			// Get the parent video container

			const videoContainer = $(this).closest('.mas_video_container');

			// Get the related video cover and fade it out

			const videoCover = videoContainer.find('.video_cover');

			videoCover.fadeOut();

			// Add a class to the video container so we can show our plyr controls

			videoContainer.addClass('show_controls');

			// Find the related plyr instance

			const plyrInstance = videoContainer.find('.plyr');

			// Get the index of this specific plyr instance within all of our plyrs on the page

			const plyrIndex = plyrInstances.index(plyrInstance);

			// Play this specific plyr instance

			players[plyrIndex].play();

			/* Listen for the ended event

			function restartPlyrInstance() {
				// Set video time back to 0
				players[plyrIndex].seek(1);
				players[plyrIndex].play();
			}

			players[plyrIndex].on('ended', (plyr) => {
				players[plyrIndex].seek(0);
  				players[plyrIndex].play();
			}); */
		});

	});

})(jQuery);