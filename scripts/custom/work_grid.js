(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		MixItUp
		Filtering the work grid by categories.
		https://www.kunkalabs.com/mixitup/docs/get-started/
		=================================*/

		if($('.work_mixitup_container').length) {
			const workMixer = mixitup('.work_mixitup_container', {
				pagination: {
			        limit: 42,
			        hidePageListIfSinglePage: true
			    },
			    templates: {
			    	pagerPrev: '<button type="button" class="${classNames}" data-page="prev"></button>',
			    	pagerNext: '<button type="button" class="${classNames}" data-page="next"></button>'
			    },
			    animation: {
			        duration: 600,
			        reverseOut: false,
			        effects: "fade"
			    }
			});

			// Before a mixitup operation has started

			$('.work_mixitup_container').on('mixStart', function() {
				// Check if there is an expanded view container and close it
				const currentExpandedView = document.querySelector('.work_expanded_view_container');
				if(currentExpandedView) {
					closeExpandedWorkView();
				}
			});

			// After a mixitup operation has completed

			$('.work_mixitup_container').on('mixEnd', function(e) {
				// Anchor the scroll position to the top of the filters.
				// Especially helpful when clicking a pagination link.
				$('.work_mixitup_container').velocity("scroll", { duration: 1000, offset: -50 });
			});
		}

		/*================================= 
		Mobile Mixitup Toggle
		=================================*/

		var filterButton = $('.mobile_mixitup_filter button');
		var filterTitle = $('.mobile_filter_title');
		var filterDrawer = $('.work_mixitup_container .mixitup_filters');
		var filterItem = $('.work_mixitup_container .mixitup_filters li');

		// On click on the mobile filter reveal/hide the filter items

		filterButton.on('click', function() {
			if(filterDrawer.hasClass('open')) {
				filterDrawer.removeClass('open');
			}
			else {
				filterDrawer.addClass('open');
			}
		});

		// On click on a filter item then change the filter title to indicate what has been filtered

		filterItem.on('click', function(){
			var itemTitle = $(this).attr('data-filter');

			// If not the all filter then capture the title without the '.'

			if(itemTitle !== 'all') {
				var itemTitle = $(this).text();
			}

			// Else is the all filter then just capture the title 'all'

			else {
				var itemTitle = $(this).attr('data-filter');
			}

			// Set the title text

			filterTitle.text(itemTitle);

			// Hide the drawer

			filterDrawer.removeClass('open');
		})

		/*================================= 
		Expanded View
		=================================*/

		// Save elements
		
		const workItems = Array.from(document.querySelectorAll('.work_item'));

		const expandedViewItems = Array.from(document.querySelectorAll('.expanded_view_item'));

		let players;

		function setupPlyr() {

			// Setup the plyr instance

			players = plyr.setup('.work_expanded_view_container', {
				controls: ['play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen']
			});

			// Listen for when plyr is ready then trigger the instance to play

			players[0].on('ready', function(event) {
			  var instance = event.detail.plyr;
			  instance.play();
			});
		}

		// Create an empty array of visible work items

		var visibleWorkItems = [];

		function closeExpandedWorkView(callback) {

			// Get the currently expanded view

			const currentExpandedView = document.querySelector('.work_expanded_view_container');

			// Pause the video

			players[0].pause();

			// Transition out the view

			currentExpandedView.classList.remove('open');

			// After the transition has finished then remove it from the dom.
			// If there is a callback then handle the callback.

			currentExpandedView.addEventListener('transitionend', function(e){
				if(e.propertyName.includes('max-height')) {
					currentExpandedView.remove();
					if(callback) {
						callback();
					}
				}
			});
		}

		function showExpandedWorkView() {

			// Create the expanded view html

			const workInfo = {
				videoType: this.dataset.videoType,
				videoId: this.dataset.videoId,
				clients: this.dataset.clients,
				categories: this.dataset.categories,
				tags: this.dataset.tags,
				link: this.dataset.link,
				content: this.dataset.content,
				director: this.dataset.director
			}

			// Helper functions to evaluate if a post has a given component. Then determine if it should be rendered.

			function hasContent(content) {
				return content ? `<p>${content}</p>` : '';
			}

			function hasCategories(categories) {
				return categories ? `<h4><span class="text_mediumgrey inline_header">Category</span> ${categories}</h4>` : '';
			}

			function hasTags(tags) {
				return tags ? `<h4><span class="text_mediumgrey inline_header">Tags</span> ${tags}</h4>` : '';
			}

			function hasClients(clients) {
				return clients ? `<h4><span class="text_mediumgrey inline_header">Client</span> ${clients}</h4>` : '';
			}

			function hasDirector(director) {
				return director ? `<h4><span class="text_mediumgrey inline_header">Director</span> ${director}</h4>` : '';
			}

			// Contents of the expanded view.

			const expandedViewContents = `
				<div class="work_expanded_view">
					<h4 class="close_expanded_view">Close X</h4>
					<div class="work_expanded_view_video">
						<div data-type="${workInfo.videoType}" data-video-id="${workInfo.videoId}"></div>
					</div>
					<div class="work_expanded_view_content">
						<div class="wysiwig">
							${hasContent(workInfo.content)}
						</div>
						<div class="mas_row">
							<div class="column_1_2">
								${hasCategories(workInfo.categories)}
								${hasTags(workInfo.tags)}
								${hasClients(workInfo.clients)}
								${hasDirector(workInfo.director)}
							</div>
							<div class="column_1_2 copy_to_clipboard">
								<h4 class="clipboard" data-clipboard-text="${workInfo.link}">Copy Project Link</h4>
							</div>
						</div>
					</div>
				</div>
			`

			// Clear array of currently visible work items

			visibleWorkItems = [];

			// Set array of currently visible work items

			workItems.forEach(item => {

				// Check if the item is visible. Non visible items have a style attribute display: none

				if(item.style.display !== 'none') {

					// Push the visible items into our array

					visibleWorkItems.push(item);
				}
			});

			// Clicked item position within our array of visible items

			const itemIndex = visibleWorkItems.indexOf(this);

			// Based on the index of the clicked item and the amount of items per row then find the item where we should append the expanded view.
			// Expanded view is always shown above the row of the clicked item.

			// Get the amount of items per row based on the parent container width divided by item width

			const itemWidth = parseFloat(window.getComputedStyle(this).width, 10);

			const parentContainerWidth = parseFloat(window.getComputedStyle(this.parentElement).width, 10);

			const itemsPerRow = Math.round(parentContainerWidth / itemWidth);

			let itemToAppendView;

			// Check the modulus of the the items index in our array of visible items to the amount of items per row.
			// Conditionally figure out which element to append the expanded view to.

			if((itemIndex % itemsPerRow) === 0) {
				itemToAppendView = this;
			}
			else if((itemIndex % itemsPerRow) === 1) {
				itemToAppendView = visibleWorkItems[itemIndex-1];
			}
			else if((itemIndex % itemsPerRow) === 2) {
				itemToAppendView = visibleWorkItems[itemIndex-2];
			}

			// Check if the expanded view is already appended in the right place

			const itemToAppendViewPreviousSibling = itemToAppendView.previousElementSibling;

			const currentExpandedView = document.querySelector('.work_expanded_view_container');

			// If there is a previous sibling and if that sibling is the expanded view container then just switch out its inner html

			if(itemToAppendViewPreviousSibling && itemToAppendViewPreviousSibling.classList.contains('work_expanded_view_container')) {
				
				// Clear the inner contents of the expanded view
				
				currentExpandedView.innerHTML = expandedViewContents;

				$('.work_expanded_view_container').velocity("scroll", { duration: 1000 });

				setupPlyr();
				
			}

			// Else open up a new expanded view

			else {

				function createNewExpandedView() {

					// Create the dom node and open the new expanded view

					const workExpandedViewNode = document.createElement('div');
				    workExpandedViewNode.innerHTML = expandedViewContents;
				    workExpandedViewNode.className = 'work_expanded_view_container';

					itemToAppendView.parentNode.insertBefore(workExpandedViewNode, itemToAppendView);

					const workExpandedView = document.querySelector('.work_expanded_view_container');

					// Trigger the expanded view animation

					window.setTimeout(function(){
						workExpandedView.classList.add('open');
					}, 300);

					// Scroll to the top of the view

					window.setTimeout(function(){
						$('.work_expanded_view_container').velocity("scroll", { duration: 500, offset: -50 })
					}, 1000);

					setupPlyr();
				}

				// Remove any currently open expanded views

				if(currentExpandedView) {

					// Close the view and on callback add the new view

					closeExpandedWorkView(createNewExpandedView);

				}
				else {
					createNewExpandedView();
				}

			}

		}

		/*---------------------------
		Events
		---------------------------*/

		// Click event on individual work item that has an expanded view.

		expandedViewItems.forEach(item => item.addEventListener('click', showExpandedWorkView)); 

		// Close click event. Using delegation because close icon is not immediatley on the page.

		document.querySelector('body').addEventListener('click', function(e) {
			if (e.target.className === 'close_expanded_view') {
				closeExpandedWorkView();
			}
		});

	});

})(jQuery);