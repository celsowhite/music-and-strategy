(function($) {

	$(document).ready(function() {

		/*================================= 
		UTILITY FUNCTIONS
		=================================*/

		// Utility function to make links of the json text.
		// Found via http://stackoverflow.com/questions/37684/how-to-replace-plain-urls-with-links.

		function linkify(inputText) {
		    var replacedText, replacePattern1, replacePattern2, replacePattern3;

		    //URLs starting with http://, https://, or ftp://
		    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
		    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

		    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
		    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
		    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

		    //Change email addresses to mailto:: links.
		    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
		    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

		    return replacedText;
		}

		// Utility to parse the twitter date

		function parseTwitterDate(tdate) {
		    var system_date = new Date(Date.parse(tdate));
		    var user_date = new Date();
		    if (K.ie) {
		        system_date = Date.parse(tdate.replace(/( \+)/, ' UTC$1'))
		    }
		    var diff = Math.floor((user_date - system_date) / 1000);
		    if (diff <= 1) {return "just now";}
		    if (diff < 20) {return diff + " seconds ago";}
		    if (diff < 40) {return "half a minute ago";}
		    if (diff < 60) {return "less than a minute ago";}
		    if (diff <= 90) {return "one minute ago";}
		    if (diff <= 3540) {return Math.round(diff / 60) + " minutes ago";}
		    if (diff <= 5400) {return "1 hour ago";}
		    if (diff <= 86400) {return Math.round(diff / 3600) + " hours ago";}
		    if (diff <= 129600) {return "1 day ago";}
		    if (diff < 604800) {return Math.round(diff / 86400) + " days ago";}
		    if (diff <= 777600) {return "1 week ago";}
		    return "on " + system_date;
		}

		// from http://widgets.twimg.com/j/1/widget.js

		var K = function () {
		    var a = navigator.userAgent;
		    return {
		        ie: a.match(/MSIE\s([^;]*)/)
		    }
		}();

		/*================================= 
		TWITTER FEED AJAX CALL
		Uses an ajax call to trigger my twitterajax wordpress function in includes/get_twitter.php.
		That function uses oauth to authorize my twitter application and sends the tweets back upon success.
		=================================*/

		// Save the div where the recent tweet will go.
		
		const recentTweetText = $('#mas_recent_tweet');

		// First check that the page has a place to output the recent tweet.

		if(recentTweetText.length) {
			$.ajax({
				url : twitterajax.ajax_url,
				type : 'get',
				data : {
					action : 'showTweets'
				},
				success : function(response) {
					// Parse the string delivered by the php file into a JS object.
					const JSONresponse = JSON.parse(response);
					// Get the first tweet / the most recent.
					const recentTweet = JSONresponse[0].text;
					// Run the text through the linkify function to covert any links.
					const linkifiedTweet = linkify(recentTweet);
					// Recent tweet date
					const recentTweetDate = JSONresponse[0].created_at;
					// Output the final text onto the page.
					recentTweetText.html('<h4>' + parseTwitterDate(recentTweetDate) + '</h4><h1>' + linkifiedTweet + '</h1>');
				}
			});
		}

	});

})(jQuery);