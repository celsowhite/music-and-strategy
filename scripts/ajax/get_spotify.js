(function($) {

	$(document).ready(function() {

		/*================================= 
		Spotify AJAX call
		Using server side authentication to authenticate with Spotify API. Check out includes/spotify/get_spotify.php
		From our server side code we are getting all the necessary data to input into the DOM.
		=================================*/

		// Only show currently listening on the homepage

		if(document.querySelector('body').classList.contains('home')) {

			$.ajax({
				url: spotifyajax.ajax_url,
				type: 'get',
				data: {
					action: 'MAS'
				},
				success: function(response) {

					// The response from our php function will send all tracks from the most recent playlist

					const tracks = JSON.parse(response)

					// Filter the tracks to just grab tracks with previews available.

					const tracksWithPreviews = tracks.filter(function(track){
						return track.track.preview_url;
					});

					// Get the total amount of tracks in this playlist so we can select a random one.
					// Subtracting 1 so that we can find a track within the array of data. Arrays start at 0 and then go to the total-1

					const totalTracks = tracksWithPreviews.length - 1;

					const randomTrackIndex = Math.floor(Math.random() * totalTracks); 

					// Save the track data

					const trackData = tracksWithPreviews[randomTrackIndex].track;

					const trackName = trackData.name;

					const trackPreviewURL = trackData.preview_url;

					const artistName = trackData.artists[0].name;

					// Set the track in the dom and the audio data attribute. Then we can trigger the play/pause functionality via another js function (Spotify Play/Pause).

					const currentlyListeningContainer = document.querySelector('.currently_listening_to');

					const currentlyListeningTrack = document.querySelector('.currently_listening_track');

					const currentlyListeningTrackName = document.querySelector('.currently_listening_track_name');

					const currentlyListeningArtist = document.querySelector('.currently_listening_artist')

					currentlyListeningContainer.innerHTML = 'Currently Listening To: <span class="currently_listening_track" data-preview="' + trackPreviewURL + '">' + trackName + ' - ' + artistName + '</span>';

				}
			});

		}

		/*================================= 
		Spotify Play/Pause
		=================================*/

		const spotifyTrigger = document.querySelector('.currently_listening_track');

		var spotifyTrack;

		function toggleSpotify(target) {

			const that = target;

			// Grab the preview url from the html element

			const previewUrl = that.dataset.preview;

			// Check if the track has already been initialized using the API https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement
			// If it has been initialized then check if it is paused.

			if(!spotifyTrack || spotifyTrack.paused) {

				// Create a new audio instance / update the current one.
				spotifyTrack = new Audio(previewUrl);

				// Play the track
				spotifyTrack.play();

				// Trigger the css classes so we can visually show the track is playing
				that.classList.toggle('playing');
			}

			// If the track is playing when the link is clicked then pause it.

			else {
				spotifyTrack.pause();
				that.classList.toggle('playing');
			}

			// Listen for when the audio has ended

			function resetSpotify() {
				spotifyTrack.currentTime = 0;
				that.classList.toggle('playing');
			}

			spotifyTrack.addEventListener('ended', resetSpotify);
		}

		// Bind the click event on the Spotify track because it doesn't exist on page load

		document.querySelector('body').addEventListener('click', function(e) {

			// If clicking on the track then run the toggleSpotify function

			if(typeof e.target.className === 'string') {
				if(e.target.className.includes('currently_listening_track')) {
					// Pass the target to the function
					toggleSpotify(e.target);
				}
			}
		});
	});

})(jQuery);