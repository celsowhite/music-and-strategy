<?php
// Inspiration load more post repeater template.
?>

<div class="column_1_2 thumbnail_block">
	<a class="image_container" href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail('square'); ?>
		<?php if(get_post_format() === 'video'): ?>
			<div class="overlay">
				<img class="play_button" src="<?php echo get_template_directory_uri() . '/img/icons/play_button_white.svg'; ?>" />
			</div>
		<?php endif; ?>
	</a>
	<header class="thumbnail_header">
		<h4 class="left"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<h4 class="text_mediumgrey right"><?php echo get_the_date( 'd.m.y' ) ?></h4>
	</header>
	<?php the_excerpt(); ?>
</div>