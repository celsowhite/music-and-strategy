<?php
/**
 * The template for displaying all single work posts.
 * Two types of work posts exists. One with regular work expanded view content. Another that is for more 
 * fleshed out collaboration posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hidden Breadcrumb Data -->

			<div class="breadcrumb_info" data-link="<?php echo get_page_link(72); ?>" data-text="Back To Work"></div>

			<!-- Page Header -->

			<?php get_template_part('template-parts/component', 'page_header'); ?>

			<!-- Page Content -->

			<div class="page_content">

				<div class="mas_panel">

					<div class="content_fade_in">

						<div class="small_container">

							<header class="title_header">
								<h1><?php the_title(); ?></h1>
							</header>

						</div>

						<?php 

							// Get the categories for this specific post

					        $categories = get_the_terms($post->ID, 'mas_work_categories');

					        // Create an array of just the category slugs

					        $slugs = wp_list_pluck($categories, 'slug');

					        // Check if this post is a strategy project. Then render the strategy format

					        if(in_array('strategy', $slugs)):
						?>

							<?php get_template_part('template-parts/content', 'strategy_post'); ?>

						<?php 

						// Else a standard video post then render the video in the same format as the expanded view.
						
						else: ?>

							<?php get_template_part('template-parts/content', 'work_post'); ?>

						<?php endif; ?>

					</div>

				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>