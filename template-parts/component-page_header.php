<?php
// Page header w/ background image.
?>

<?php if(get_field('header_image')): ?>
	<header class="page_header mas_header" style="background-image:url(<?php echo image_id_to_url(get_field('header_image'), 'large'); ?>); background-position:<?php echo get_field('header_image_horizontal_focal_point') . ' ' . get_field('header_image_vertical_focal_point'); ?>;"></header>	
<?php endif; ?>