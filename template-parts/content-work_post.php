<?php 
/*---------------------
Single work post that is not a collaboration.
This mimicks the post content in the expanded view. Another way to access a work post directly.
This is not a template part because the expanded view is created via JS. If there was a way
to share templates between js and php that would be awesome!
Show three related work pieces by category at the bottom.
---------------------*/
?>

<div class="small_container">

	<div class="work_expanded_view">
		<div class="work_expanded_view_video">

			<!-- Featured Video -->

			<section class="mas_video_container">
				<div class="video_cover">
					<div class="content">
						<img class="play_video" src="<?php echo get_template_directory_uri() . '/img/icons/play_button_white.svg'; ?>" />
					</div>
				</div>
				<div data-type="<?php the_field('video_embed_type'); ?>" data-video-id="<?php the_field('video_embed_id'); ?>"></div>
			</section>

		</div>
		<div class="work_expanded_view_content">
			<?php

			// Get the categories for this post

	        $categories = category_terms_list($post->ID, 'mas_work_categories');

			// Get the tags for this post

	        $tags = category_terms_list($post->ID, 'mas_work_tags');
			
			// Get the clients for this post

	        $clients = category_terms_list($post->ID, 'mas_work_clients');

	        ?>
	        <?php if(get_field('work_content')): ?>
	        	<p><?php the_field('work_content'); ?></p>
	        <?php endif; ?>
	        <div class="mas_row">
				<div class="column_1_2">
					<?php if(get_the_terms($post->ID, 'mas_work_categories')): ?>
						<h4><span class="text_mediumgrey inline_header">Category</span> <?php echo $categories; ?></h4>
					<?php endif; ?>
					<?php if(get_the_terms($post->ID, 'mas_work_tags')): ?>
						<h4><span class="text_mediumgrey inline_header">Tags</span> <?php echo $tags; ?></h4>
					<?php endif; ?>
					<?php if(get_the_terms($post->ID, 'mas_work_clients')): ?>
						<h4><span class="text_mediumgrey inline_header">Client</span> <?php echo $clients; ?></h4>
					<?php endif; ?>
					<?php if(get_field('work_director')): ?>
						<h4><span class="text_mediumgrey inline_header">Director</span> <?php the_field('work_director'); ?></h4>
					<?php endif; ?>
				</div>
				<div class="column_1_2 copy_to_clipboard">
					<h4 class="clipboard" data-clipboard-text="<?php the_permalink(); ?>">Copy Project Link</h4>
				</div>
			</div>
		</div>
	</div>

	<?php the_content(); ?>

</div>

<!-- Related Posts -->

<div class="mas_panel">

	<div class="container">

		<?php

		$primary_post_term = get_the_terms($post->ID, 'mas_work_categories')[0];

		// Query 3 related work posts

		$related_posts_args = array (
			'post_type'           => 'mas_work', 
			'posts_per_page'      => 3,
			// Get posts within the same category
			'mas_work_categories' => $primary_post_term->slug,
			// Don't show the current post
			'post__not_in'        => array($post->ID),
			'orderby'             => 'rand' 
		);
		$related_posts_loop = new WP_Query($related_posts_args);
		if($related_posts_loop -> have_posts()): ?>

			<h1 class="title_header">More <?php echo $primary_post_term->name; ?></h1>

			<div class="mas_row work_grid">
				
				<?php while($related_posts_loop -> have_posts()): $related_posts_loop -> the_post(); ?>
					<?php get_template_part('template-parts/card', 'work_item'); ?>

				<?php endwhile; ?> 
			
			</div>

		<?php wp_reset_postdata(); endif; ?>

	</div>

</div>