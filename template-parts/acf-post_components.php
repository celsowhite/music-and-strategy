<?php if(have_rows('post_components')): while(have_rows('post_components')): the_row(); ?>

	<?php 

	// Full Width Content

	if(get_row_layout() == 'full_width_content'): ?>

		<div class="acf_component_panel wysiwyg">

			<?php the_sub_field('content'); ?>

		</div>

	<?php 

	// Centered Content

	elseif(get_row_layout() == 'centered_content'): ?>

		<div class="acf_component_panel">

			<div class="centered_content_container wysiwyg">

				<?php the_sub_field('content'); ?>

			</div>

		</div>

	<?php 

	// Image Gallery

	elseif(get_row_layout() == 'image_gallery'): ?>

		<div class="acf_component_panel image_gallery">
			<?php 
			$images = get_sub_field('images');
			if( $images ): ?>
			    <div class="mas_row">
			        <?php foreach( $images as $image ): ?>
			        	<div class="column_1_<?php the_sub_field('image_gallery_columns'); ?>">
			        		<img src="<?php echo $image['sizes']['mas_thumbnail']; ?>" />
			        	</div>
			        <?php endforeach; ?>
			    </div>
			<?php endif; ?>
		</div>

	<?php 

	// Slider

	elseif(get_row_layout() == 'slider'): ?>

		<div class="acf_component_panel">
			<?php if(have_rows('slides')): ?>
				<div class="flexslider mas_slider">
					<ul class="slides">
						<?php while(have_rows('slides')): the_row(); ?>
							<li><img src="<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>" /></li>
						<?php endwhile; ?>
					</ul>
				</div>
				<div class="flexslider caption_slider">
					<ul class="slides">
						<?php while(have_rows('slides')): the_row(); ?>
							<li><?php the_sub_field('caption'); ?></li>
						<?php endwhile; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>

	<?php 

	// Plyr Video

	elseif(get_row_layout() == 'video'): ?>

		<div class="acf_component_panel">
			
			<section class="mas_video_container">
				<div class="video_cover" style="background-image:url(<?php the_field('reel_placeholder_image'); ?>);">
					<div class="content">
						<h1><?php the_field('reel_header'); ?></h1>
						<img class="play_video" src="<?php echo get_template_directory_uri() . '/img/icons/play_button_white.svg'; ?>" />
					</div>
				</div>
				<div data-type="<?php the_sub_field('video_embed_type'); ?>" data-video-id="<?php the_sub_field('video_embed_id'); ?>"></div>
			</section>

		</div>

	<?php endif; ?>

<?php endwhile; endif; ?>