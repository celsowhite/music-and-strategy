<?php

/*---------------------------
Used to output work items on the Home, About and Work pages.
Outputs each work item so that it can function with the mixitup plugin
and the custom expanded view functionality.
---------------------------*/

// Get the categories for this specific post

$categories = get_the_terms($post->ID, 'mas_work_categories');

$categoriesHTML = htmlspecialchars(category_terms_list($post->ID, 'mas_work_categories'));

$strategyPost = false;

if($categories) {

	// Create an array of just the category slugs

	$slugs = wp_list_pluck($categories, 'slug');

	// Stringify the array of category slugs to output into the items class and work with mixitup

	$class_names = join(' ', $slugs);

	// Check if this is a 'collaboration' post

	$strategyPost = in_array('strategy', $slugs);
}

// Get the clients for this post

$clients = htmlspecialchars(category_terms_list($post->ID, 'mas_work_clients'));

// Get the tags for this post

$tags = htmlspecialchars(category_terms_list($post->ID, 'mas_work_tags'));

// Get the content for this post

$content = htmlspecialchars(get_field('work_content'));

?>

<?php 
// If a regular work post then should be able to trigger expanded view.
if(!$strategyPost): ?>

	<div class="column_1_3 mix work_item expanded_view_item <?php if ($class_names) { echo ' ' . $class_names;} ?>" data-video-type="<?php the_field('video_embed_type'); ?>" data-video-id="<?php the_field('video_embed_id'); ?>" data-clients="<?php echo $clients; ?>" data-categories="<?php echo $categoriesHTML; ?>" data-tags="<?php echo $tags; ?>" data-content="<?php echo $content; ?>" data-link="<?php the_permalink(); ?>" data-director="<?php the_field('work_director'); ?>">
		<div class="work_item_image_container">
			<?php the_post_thumbnail('mas_thumbnail'); ?>
			<div class="overlay">
				<div class="overlay_content">
					<?php the_title(); ?>
				</div>
				<img src="<?php echo get_template_directory_uri() . '/img/icons/play_button_black.svg'; ?>" />
			</div>
		</div>
		<h4><?php echo category_terms_list($post->ID, 'mas_work_clients', false); ?></h4>
	</div>

<?php 
// Else a strategy post then links to a strategy page.
else: ?>

	<div class="column_1_3 mix work_item <?php if ($class_names) { echo ' ' . $class_names;} ?>">
		<a href="<?php the_permalink(); ?>" class="work_item_image_container">
			<?php the_post_thumbnail('mas_thumbnail'); ?>
			<div class="overlay">
				<div class="overlay_content">
					<?php the_title(); ?>
				</div>
				<img class="arrow" src="<?php echo get_template_directory_uri() . '/img/icons/arrow_right_open_black.svg'; ?>" />
			</div>
		</a>
		<h4><a href="<?php the_permalink(); ?>"><?php echo category_terms_list($post->ID, 'mas_work_clients', false); ?></a></h4>
	</div>

<?php endif; ?>