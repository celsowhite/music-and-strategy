<?php
// Template to output MAS social profile icons and links.
?>

<?php if(have_rows('mas_social_profiles', 'option')): ?>
	<ul class="social_icons">
		<?php while(have_rows('mas_social_profiles', 'option')): the_row(); ?>
			<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><i class="fa fa-<?php echo sanitize_title(get_sub_field('name')); ?>"></i></a></li>
		<?php endwhile; ?>
	</ul>
<?php endif ;?>