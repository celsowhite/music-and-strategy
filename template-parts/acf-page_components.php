<?php if(have_rows('page_components')): while(have_rows('page_components')): the_row(); ?>

	<?php 
	
	// Full Width Content

	if(get_row_layout() == 'full_width_content'): ?>

		<div class="acf_component_panel">

			<?php if(get_sub_field('title')): ?>

				<div class="mas_row sidebar_layout">
					<div class="column_1_4">
						<h5><?php the_sub_field('title'); ?></h5>
					</div>
					<div class="column_3_4 wysiwyg">
						<?php the_sub_field('content'); ?>
					</div>
				</div>

			<?php else: ?>

				<?php the_sub_field('content'); ?>

			<?php endif; ?>

		</div>

	<?php 

	// Two Column Content

	elseif(get_row_layout() == 'two_column_content'): ?>

		<?php if(get_sub_field('title')): ?>	

			<div class="acf_component_panel">
				<div class="mas_row sidebar_layout">
					<div class="column_1_4">
						<h5><?php the_sub_field('title'); ?></h5>
					</div>
					<div class="column_3_4">
						<div class="mas_row">
							<?php if(have_rows('blocks')): while(have_rows('blocks')): the_row(); ?>
								<div class="column_1_2 wysiwyg">
									<?php the_sub_field('content'); ?>
								</div>
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>

		<?php else: ?>

			<div class="acf_component_panel">
				<div class="mas_row">
					<?php if(have_rows('blocks')): while(have_rows('blocks')): the_row(); ?>
						<div class="column_1_2 wysiwyg">
							<?php the_sub_field('content'); ?>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>

		<?php endif; ?>

	<?php 
	
	// Logo Grid

	elseif(get_row_layout() == 'logo_grid'): ?>

		<?php if(get_sub_field('title')): ?>

			<div class="acf_component_panel">
				<div class="mas_row sidebar_layout">
					<div class="column_1_4">
						<h5><?php the_sub_field('title'); ?></h5>
					</div>
					<div class="column_3_4">
						<?php if(get_sub_field('content')): ?>
							<?php the_sub_field('content'); ?>
						<?php endif; ?>
						<?php if(have_rows('logos')): ?>
						    <ul class="logo_grid">
						    	<?php while(have_rows('logos')): the_row(); ?>
						        	<li>
						        		<?php if(get_sub_field('link')): ?>
						        			<a href="<?php the_sub_field('link'); ?>"><img src="<?php echo image_id_to_url(get_sub_field('logo'), 'medium'); ?>" /></a>
						        		<?php else: ?>
						        			<img src="<?php echo image_id_to_url(get_sub_field('logo'), 'medium'); ?>" />
						        		<?php endif; ?>
						        	</li>
						        <?php endwhile; ?>
						    </ul>
						<?php endif; ?>
					</div>
				</div>
			</div>

		<?php else: ?>

			<div class="acf_component_panel">
				<?php if(get_sub_field('content')): ?>
					<?php the_sub_field('content'); ?>
				<?php endif; ?>
				<?php if(have_rows('logos')): ?>
				    <ul class="logo_grid">
				    	<?php while(have_rows('logos')): the_row(); ?>
				        	<li>
				        		<?php if(get_sub_field('link')): ?>
				        			<a href="<?php the_sub_field('link'); ?>"><img src="<?php echo image_id_to_url(get_sub_field('logo'), 'medium'); ?>" /></a>
				        		<?php else: ?>
				        			<img src="<?php echo image_id_to_url(get_sub_field('logo'), 'medium'); ?>" />
				        		<?php endif; ?>
				        	</li>
				        <?php endwhile; ?>
				    </ul>
				<?php endif; ?>
			</div>

		<?php endif; ?>

	<?php 

	// Image Gallery

	elseif(get_row_layout() == 'image_gallery'): ?>

		<?php if(get_sub_field('title')): ?>

			<div class="acf_component_panel image_gallery">
				<div class="mas_row sidebar_layout">
					<div class="column_1_4">
						<h5><?php the_sub_field('title'); ?></h5>
					</div>
					<div class="column_3_4">
						<?php 
						$images = get_sub_field('images');
						if( $images ): ?>
						    <div class="mas_row">
						        <?php foreach( $images as $image ): ?>
						        	<div class="column_1_<?php the_sub_field('image_gallery_columns'); ?>">
						        		<img src="<?php echo $image['sizes']['mas_thumbnail']; ?>" />
						        	</div>
						        <?php endforeach; ?>
						    </div>
						<?php endif; ?>
					</div>
				</div>
			</div>

		<?php else: ?>

			<div class="acf_component_panel image_gallery">
				<?php 
				$images = get_sub_field('images');
				if( $images ): ?>
				    <div class="mas_row">
				        <?php foreach( $images as $image ): ?>
				        	<div class="column_1_<?php the_sub_field('image_gallery_columns'); ?>">
				        		<img src="<?php echo $image['sizes']['mas_thumbnail']; ?>" />
				        	</div>
				        <?php endforeach; ?>
				    </div>
				<?php endif; ?>
			</div>

		<?php endif; ?>

	<?php 

	// Slider

	elseif(get_row_layout() == 'slider'): ?>

		<?php if(get_sub_field('title')): ?>

			<div class="acf_component_panel">

				<div class="mas_row sidebar_layout">
					<div class="column_1_4">
						<h5><?php the_sub_field('title'); ?></h5>
					</div>
					<div class="column_3_4">
						<?php if(get_sub_field('content')): ?>
							<?php the_sub_field('content'); ?>
						<?php endif; ?>
						<?php if(have_rows('slides')): ?>
							<div class="flexslider mas_slider">
								<ul class="slides">
									<?php while(have_rows('slides')): the_row(); ?>
										<li><img src="<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>" /></li>
									<?php endwhile; ?>
								</ul>
							</div>
							<div class="flexslider caption_slider">
								<ul class="slides wysiwyg">
									<?php while(have_rows('slides')): the_row(); ?>
										<li><?php the_sub_field('caption'); ?></li>
									<?php endwhile; ?>
								</ul>
							</div>
						<?php endif; ?>
					</div>
				</div>

			</div>

		<?php else: ?>

			<div class="acf_component_panel">

				<?php if(get_sub_field('content')): ?>
					<?php the_sub_field('content'); ?>
				<?php endif; ?>
				<?php if(have_rows('slides')): ?>
					<div class="flexslider mas_slider">
						<ul class="slides">
							<?php while(have_rows('slides')): the_row(); ?>
								<li><img src="<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>" /></li>
							<?php endwhile; ?>
						</ul>
					</div>
					<div class="flexslider caption_slider">
						<ul class="slides">
							<?php while(have_rows('slides')): the_row(); ?>
								<li><?php the_sub_field('caption'); ?></li>
							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>

			</div>

		<?php endif; ?>

	<?php endif; ?>

<?php endwhile; endif; ?>