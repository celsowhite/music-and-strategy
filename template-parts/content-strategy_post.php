<?php 
/*---------------------
Single strategy post content.
They can use our set post components in the backend to create a unique layout for each.
At the bottom of the post we query more collaboartions.
---------------------*/
?>

<div class="small_container wysiwig">

	<!-- Post Content -->

	<?php get_template_part('template-parts/acf', 'post_components'); ?>

	<!-- Tags/Categories -->

	<?php if(get_the_terms($post->ID, 'mas_work_categories')): ?>
		<h4><span class="text_mediumgrey inline_header">Category</span> <?php echo category_terms_list($post->ID, 'mas_work_categories'); ?></h4>
	<?php endif; ?>

	<?php if(get_the_terms($post->ID, 'mas_work_tags')): ?>
		<h4><span class="text_mediumgrey inline_header">Tags</span> <?php echo category_terms_list($post->ID, 'mas_work_tags'); ?></h4>
	<?php endif; ?>

	<?php if(get_the_terms($post->ID, 'mas_work_clients')): ?>
		<h4><span class="text_mediumgrey inline_header">Client</span> <?php echo category_terms_list($post->ID, 'mas_work_clients'); ?></h4>
	<?php endif; ?>

</div>

<?php

// Query 3 more collaborations

$more_collaborations_args = array (
	'post_type'           => 'mas_work', 
	'posts_per_page'      => 3,
	'mas_work_categories' => 'strategy',
	'orderby'             => 'rand',
	// Don't show the current post
	'post__not_in'        => array($post->ID)
);
$more_collaborations_loop = new WP_Query($more_collaborations_args);
if($more_collaborations_loop -> have_posts()): ?>

	<div class="mas_panel">

		<div class="container">

			<h1 class="title_header large">More Strategy</h1>
			
			<div class="mas_row work_grid">

				<?php while($more_collaborations_loop -> have_posts()): $more_collaborations_loop -> the_post(); ?>
					<?php get_template_part('template-parts/card', 'work_item'); ?>
				<?php endwhile; ?> 

			</div>

			<div class="text_center">
				<a class="arrow_link black" href="<?php echo get_page_link(72); ?>">View All Work</a>
			</div>
		</div>

	</div>

<?php wp_reset_postdata(); endif; ?>