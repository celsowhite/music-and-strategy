<?php
/**
 * The template for displaying 404 pages (not found).
 */

get_header(); ?>

	<main class="main_wrapper">

		<!-- Hidden Breadcrumb Data -->

		<div class="breadcrumb_info" data-link="<?php echo esc_url( home_url( '/' ) ); ?>" data-text="Back To Main"></div>

		<!-- Page Content -->

		<div class="page_content">

			<div class="small_container wysiwig content_fade_in">

				<div class="mas_panel">

					<h1>Page Not Found</h1>

				</div>
			</div>

		</div>

	</main>

<?php get_footer(); ?>
