<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hidden Breadcrumb Data -->

			<div class="breadcrumb_info" data-link="<?php echo get_page_link(74); ?>" data-text="Back To Feed"></div>

			<!-- Page Header -->

			<?php get_template_part('template-parts/component', 'page_header'); ?>

			<!-- Page Content -->

			<div class="page_content">

				<div class="small_container content_fade_in">

					<div class="mas_panel">

						<!-- Date/Title -->

						<header class="title_header">
							<h4><?php echo get_the_date( 'd.m.y' ) ?></h4>
							<h1><?php the_title(); ?></h1>
						</header>

						<!-- Content -->

						<div class="wysiwig">
							<?php get_template_part('template-parts/acf', 'post_components'); ?>
						</div>

						<!-- Tags/Categories -->

						<?php 
						if(get_the_terms($post->ID, 'category')): 
						$category_count = count(get_the_terms($post->ID, 'category'));
						if($category_count > 1) {
							$category_title = 'Categories';
						}
						else {
							$category_title = 'Category';
						}
						?>
							<h4><span class="text_mediumgrey inline_header"><?php echo $category_title; ?></span> <?php echo category_terms_list($post->ID, 'category'); ?></h4>
						<?php endif; ?>

						<?php 
						if(get_the_terms($post->ID, 'post_tag')): 
						$tag_count = count(get_the_terms($post->ID, 'post_tag'));
						if($tag_count > 1) {
							$tag_title = 'Tags';
						}
						else {
							$tag_title = 'Tag';
						}
						?>
							<h4><span class="text_mediumgrey inline_header"><?php echo $tag_title; ?></span> <?php echo category_terms_list($post->ID, 'post_tag'); ?></h4>
						<?php endif; ?>

					</div>

				</div>

				<!-- Related Posts -->

				<?php

				// Get the current posts categories and save them into an array of ids so we can query related posts
				
				$current_post_category_ids = array();
				$current_post_categories = get_the_category();
				foreach($current_post_categories as $category) {
				  array_push($current_post_category_ids, $category->term_id);
				};

				// Query 3 related posts

				$related_posts_args = array (
					'post_type' => 'post', 
					'posts_per_page' => 2,
					// Don't show the current post
					'post__not_in' => array($post->ID), 
					'category__in' => $current_post_category_ids
				);
				$related_posts_loop = new WP_Query($related_posts_args);
				if($related_posts_loop -> have_posts()): ?>

					<div class="mas_panel">
						<div class="small_container">
							<h1 class="title_header">Related Posts</h1>
							
							<div class="mas_row">

								<?php while($related_posts_loop -> have_posts()): $related_posts_loop -> the_post(); ?>
									<?php get_template_part('template-parts/card', 'inspiration_post'); ?>
								<?php endwhile; ?> 

							</div>
						</div>
					</div>

				<?php wp_reset_postdata(); endif; ?>

			</div>

		<?php endwhile; ?>

	</main>
	
<?php get_footer(); ?>