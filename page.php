<?php
/*
Default page template
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Hidden Breadcrumb Data -->

			<div class="breadcrumb_info" data-link="<?php echo esc_url( home_url( '/' ) ); ?>" data-text="Back To Main"></div>

			<!-- Page Header -->

			<?php get_template_part('template-parts/component', 'page_header'); ?>

			<!-- Page Content -->

			<div class="page_content">

				<div class="small_container wysiwig content_fade_in">

					<div class="mas_panel">

						<?php get_template_part('template-parts/acf', 'page_components'); ?>

					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

