<?php
/**
 * The template for displaying archive pages.
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<!-- Page Content -->

		<div class="page_content">

			<div class="content_fade_in">

				<div class="mas_panel">

					<?php if ( have_posts() ) : ?>

						<div class="small_container">
							<div class="centered_content_container">
								<header class="title_header">
									<h1><?php single_term_title(); ?></h1>
									<p><?php the_archive_description(); ?></p>
								</header>
							</div>
						</div>

						<?php 
						// If is one of the work custom taxonomies then show the grid of work items w/ expanded views
						if(is_tax('mas_work_categories') || is_tax('mas_work_clients') || is_tax('mas_work_tags')): ?>

							<!-- Hidden Breadcrumb Data -->

							<div class="breadcrumb_info" data-link="<?php echo get_page_link(72); ?>" data-text="View All Work"></div>

							<div class="container">

								<div class="mas_row work_grid">

									<?php while ( have_posts() ) : the_post(); ?>

										<?php get_template_part('template-parts/card', 'work_item'); ?>

									<?php endwhile; ?>

								</div>

							</div>

						<?php 
						// Else is an archive for inspiration posts
						else: ?>

							<!-- Hidden Breadcrumb Data -->

							<div class="breadcrumb_info" data-link="<?php echo get_page_link(74); ?>" data-text="View All Posts"></div>

							<div class="small_container">

								<div class="mas_row">

									<?php while ( have_posts() ) : the_post(); ?>

										<?php get_template_part('template-parts/card', 'inspiration_post'); ?>

									<?php endwhile; ?>

								</div>

							</div>

						<?php endif; ?>

					<?php endif; ?>

					<?php 
					// Numbered pagination
					// Current amount per archive page is 30
					// This number is set in the backend Settings > Reading
					echo mas_pagination($wp_query->max_num_pages); ?>

				</div>

			</div>

		</div>

	</main>

<?php get_footer(); ?>