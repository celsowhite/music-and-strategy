<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div>

<footer class="footer">

	<div class="container footer_logo_container parallax_item" data-speed="1.7">
		<img src="<?php echo get_template_directory_uri() . '/img/logo/logo_large_black.svg' ?>" class="footer_logo" />
	</div>

	<div class="main_footer">

		<div class="container">

			<!-- Footer Top w/ Social -->

			<div class="footer_top">
				<?php get_template_part('template-parts/component', 'social_profiles'); ?>
			</div>

			<!-- Footer Bottom -->

			<div class="footer_bottom">

				<!-- Contact Info -->

				<ul class="footer_contact">
					<?php if(have_rows('mas_addresses', 'option')): while(have_rows('mas_addresses', 'option')): the_row(); ?>
						<li>
							<h4><?php the_sub_field('location'); ?></h4>
							<p><?php the_sub_field('address'); ?></p>
						</li>
					<?php endwhile; endif; ?>
					<li>
						<h4>Contact Us</h4>
						<p><a href="mailto:<?php the_field('mas_email', 'option'); ?>"><?php the_field('mas_email', 'option'); ?></a></p>
					</li>
				</ul>

				<!-- Copyright -->
				
				<div class="footer_copyright">
					<p>© <?php echo Date('Y'); ?> MAS Music And Strategy LLC. All rights reserved.</p>
				</div>

			</div>

		</div>

	</div>

</footer>

<?php wp_footer(); ?>

<script>

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-103584446-1', 'auto');
	ga('send', 'pageview');

</script>

</body>
</html>
